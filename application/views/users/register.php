<?php echo validation_errors('<p class="alert alert-dismissable alert-danger">'); ?>
<?php echo form_open('users/register'); ?>

<p>
	<?php echo form_label('First name:'); ?>
	<?php
	$data = array(
				'name' => 'first_name',
				'value' => set_value('first_name')
				);
	?>
	<?php echo form_input($data); ?>
</p>

<p>
	<?php echo form_label('Last name:'); ?>
	<?php
	$data = array(
				'name' => 'last_name',
				'value' => set_value('last name')
				);
	?>
	<?php echo form_input($data); ?>
</p>

<p>
	<?php echo form_label('Email address:'); ?>
	<?php
	$data = array(
				'name' => 'email',
				'value' => set_value('email')
				);
	?>
	<?php echo form_input($data); ?>
</p>

<p>
	<?php echo form_label('Username'); ?>
	<?php
	$data = array(
				'name' => 'username',
				'value' => set_value('username')
				);
	?>
	<?php echo form_input($data); ?>
</p>

<p>
	<?php echo form_label('Password'); ?>
	<?php
	$data = array(
				'name' => 'password',
				'value' => set_value('password')
				);
	?>
	<?php echo form_password($data); ?>
</p>

<p>
	<?php echo form_label('Confirm password'); ?>
	<?php
	$data = array(
				'name' => 'password2',
				'value' => set_value('confirm password')
				);
	?>
	<?php echo form_password($data); ?>
</p>

<p>
	
	<?php
	$data = array(
				'name' => 'submit',
				'class' => 'btn btn-primary',
				'value' => 'Login'
				);
	?>
	<?php echo form_submit($data); ?>
</p>

<?php echo form_close(); ?>